package com.altran.hackaton.be.data;

import java.util.ArrayList;
import java.util.List;

import com.altran.hackaton.be.dao.*;
import com.altran.hackaton.be.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class SampleData {
	
	@Autowired
	private RestaurantRepository restRepo;
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private CarRepository carRepo;

	@Autowired
	private OrderRepository orderRepo;

	@Autowired
	private ItemRepository itemRepo;

	@Autowired
	private MenuRepository menuRepo;
	
	@Autowired
	private PasswordEncoder encoder;
	
	@EventListener
    public void appReady(ApplicationReadyEvent event) {		
		for (Restaurant restaurant: initializeRestaurants()) {
			restRepo.save(restaurant);
		}		
		for (User user: initializeUsers()) {
			userRepo.save(user);
		}
		for (Car car: initializeCars()) {
			carRepo.save(car);
		}
		for (Order order: initializeOrder()) {
			orderRepo.save(order);
		}
		for (Item item: initializeItem()) {
			itemRepo.save(item);
		}
		for (RestaurantMenu menu: initializeMenu()) {
			menuRepo.save(menu);
		}
	}
	
	private List<User> initializeUsers(){
		List<User> users = new ArrayList<>();
		
		User user = new User();
		user.setId(1);
		user.setLogin("JOHN");
		user.setPassword(encoder.encode("Wick3"));		
		users.add(user);

		return users;
	}
	
	private List<Car> initializeCars(){
		
		List<Car> cars = new ArrayList<>();
		
		Car car = new Car();
		car.setIdentifier(1);
		car.setPassword(encoder.encode("CHEV2345"));
		car.setToken("XA23546437HYI23BGT657U79");
		car.setUsername("JOHN");
		cars.add(car);
		
		return cars;
	}

	private List<Restaurant> initializeRestaurants() {
		
		List<Restaurant> list = new ArrayList<>();
		
		Restaurant rest = new Restaurant();
		rest.setAddress("Avenida Central");
		rest.setIdentifier("ABC");
		rest.setName("Android Food");
		rest.setLatitude(31.222);
		rest.setLongitude(74.222);
		list.add(rest);

		Restaurant rest1 = new Restaurant();
		rest1.setAddress("Avenida Duque de Caxias");
		rest1.setIdentifier("DQA");
		rest1.setName("Android Food Park");
		rest1.setLatitude(32.272);
		rest1.setLongitude(114.292);
		list.add(rest1);

		return list;
	}
	private List<Order> initializeOrder() {
		List<Order> list = new ArrayList<>();
		return list;


	}
	private List<RestaurantMenu> initializeMenu() {
		List<RestaurantMenu> list = new ArrayList<>();
		RestaurantMenu menu = new RestaurantMenu();
		menu.setIdentifier("1");
		menu.setRestaurantId("ABC");
		menu.setImage("menu.png");
		menu.setName("Menu 1");
		menu.setValue(10.2);
		list.add(menu);
		menu = new RestaurantMenu();
		menu.setIdentifier("3");
		menu.setRestaurantId("ABC");
		menu.setImage("menu3.png");
		menu.setName("Menu 3");
		menu.setValue(13.2);
		list.add(menu);
		menu = new RestaurantMenu();
		menu.setIdentifier("2");
		menu.setRestaurantId("DQA");
		menu.setImage("menu2.png");
		menu.setName("Menu 2");
		menu.setValue(11.2);
		list.add(menu);
		return list;

	}

	private List<Item> initializeItem() {
		List<Item> list = new ArrayList<>();

		return list;

	}

}
