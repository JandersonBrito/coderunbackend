package com.altran.hackaton.be.controller;

import com.altran.hackaton.be.dto.MenuRepresentation;
import com.altran.hackaton.be.dto.RestaurantRepresentation;
import com.altran.hackaton.be.service.MenuService;
import com.altran.hackaton.be.service.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class MenuController {
	
	@Autowired
	private MenuService menuService;

	@GetMapping({"/api/food/restaurant"})
	public ResponseEntity<List<MenuRepresentation>> listaRestaurants(@RequestParam String token,
			@RequestParam String restaurantId) {
		List<MenuRepresentation> lista = menuService.listMenu(token, restaurantId);
		return lista.isEmpty() ? new ResponseEntity<>(HttpStatus.NO_CONTENT)
				: new ResponseEntity<List<MenuRepresentation>>(lista, HttpStatus.OK);
	}

/*	@GetMapping({"/api/food/restaurants/orders"})
	public void getRestaurantOrder(){
	}
 */

}
