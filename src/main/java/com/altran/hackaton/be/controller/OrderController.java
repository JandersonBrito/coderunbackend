package com.altran.hackaton.be.controller;

import com.altran.hackaton.be.dto.ItemRepresentation;
import com.altran.hackaton.be.dto.OrderRepresentation;
import com.altran.hackaton.be.dto.RestaurantRepresentation;
import com.altran.hackaton.be.model.Order;
import com.altran.hackaton.be.service.ItemService;
import com.altran.hackaton.be.service.OrderService;
import com.altran.hackaton.be.service.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;
    @Autowired
    private ItemService itemService;

    @PostMapping({"/api/food/createOrder"})
   public ResponseEntity<String> createOrder(@RequestParam String token, @RequestBody OrderRepresentation order){
          //List<OrderRepresentation> lista = orderService.listOrder(token);
          String i = UUID.randomUUID().toString();
          orderService.save(i,order);
          return new ResponseEntity<String>(order.getOrderId(), HttpStatus.OK);

    }

    @PostMapping({"/api/food/cancelOrder"})
    public ResponseEntity<OrderRepresentation> cancelOrder(@RequestParam String token,@RequestParam String orderId){
        return(updateOrder(token,orderId,"CANCELED"));

    }

   @PutMapping({"/api/food/updateOrder"})
   public ResponseEntity<OrderRepresentation> updateOrder
           (@RequestParam String token,@RequestParam String orderId,@RequestParam String status) {
       OrderRepresentation lista = orderService.findOrder(token, orderId).get(0);
       if (lista.equals(null)) {
           new ResponseEntity<>(HttpStatus.NO_CONTENT);
       }
       lista.setStatus(status);
       orderService.save(lista);
       return new ResponseEntity<OrderRepresentation>(lista, HttpStatus.OK);
   }

    @GetMapping({"/api/food/getStatusEncomenda"})
        public ResponseEntity<String> getStatusEncomenda
            (@RequestParam String token,@RequestParam String orderId){
        OrderRepresentation lista = orderService.findOrder(token,orderId).get(0);
        return lista.equals(null) ? new ResponseEntity<>(HttpStatus.NO_CONTENT)
                : new ResponseEntity<String>(lista.getStatus(), HttpStatus.OK);
    }

    //testing
    @GetMapping({"/api/food/getOrders"})
    public ResponseEntity<List<OrderRepresentation>> getOrder(@RequestParam String token){
        List<OrderRepresentation> lista = orderService.listOrder(token);
        return lista.isEmpty() ? new ResponseEntity<>(HttpStatus.NO_CONTENT)
                : new ResponseEntity<List<OrderRepresentation>>(lista, HttpStatus.OK);

    }

    @GetMapping({"/api/food/getOrdersUser"})
    public ResponseEntity<List<OrderRepresentation>> getOrderUser(@RequestParam String token,@RequestParam String userId){
        List<OrderRepresentation> lista = orderService.listOrder(token,userId);
        return lista.isEmpty() ? new ResponseEntity<>(HttpStatus.NO_CONTENT)
                : new ResponseEntity<List<OrderRepresentation>>(lista, HttpStatus.OK);

    }

}
