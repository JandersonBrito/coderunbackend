package com.altran.hackaton.be.controller;

import java.util.List;
import java.util.UUID;

import com.altran.hackaton.be.dto.ItemRepresentation;
import com.altran.hackaton.be.dto.MenuRepresentation;
import com.altran.hackaton.be.dto.OrderRepresentation;
import com.altran.hackaton.be.model.Order;
import com.altran.hackaton.be.service.ItemService;
import com.altran.hackaton.be.service.MenuService;
import com.altran.hackaton.be.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.altran.hackaton.be.dto.RestaurantRepresentation;
import com.altran.hackaton.be.service.RestaurantService;

@RestController
public class RestaurantController {
	
	@Autowired
	private RestaurantService restaurantService;
	@Autowired
	private OrderService orderService;
	@Autowired
	private ItemService itemService;
	@Autowired
	private MenuService menuService;


	@GetMapping({"/api/food/restaurants"})
	public ResponseEntity<List<RestaurantRepresentation>> listaRestaurants(@RequestParam String token, 
			@RequestParam double x, @RequestParam double y) {		

		List<RestaurantRepresentation> lista = restaurantService.getAllRestaurants(x,y);
		for(RestaurantRepresentation li: lista){
			List<MenuRepresentation> menulista = menuService.listMenu(token, li.getRestaurantId());
			li.setMenus(menulista);
		}
		if(lista != null){
			return lista.isEmpty() ? new ResponseEntity<>(HttpStatus.NO_CONTENT)
					: new ResponseEntity<List<RestaurantRepresentation>>(lista, HttpStatus.OK);
		}else{
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);

		}

	}

/*

	@GetMapping({"/api/food/restaurants/orders"})
	public ResponseEntity<List<ItemRepresentation>> getRestaurantOrder(@RequestParam String token,@RequestParam String idOrder){
		OrderRepresentation or = orderService.findOrder(token,idOrder).get(0);
		List<ItemRepresentation> lista = itemService.listMenu(token, idOrder);
		return lista.isEmpty() ? new ResponseEntity<>(HttpStatus.NO_CONTENT)
				: new ResponseEntity<List<ItemRepresentation>>(lista, HttpStatus.OK);

	}

	@PostMapping({"/api/food/restaurants/createOrder"})
	public ResponseEntity<OrderRepresentation> createOrder
			(@RequestParam String token, @RequestBody OrderRepresentation order){
		String i = UUID.randomUUID().toString();
		orderService.save(i,order);
		return new ResponseEntity<OrderRepresentation>(order, HttpStatus.OK);

	}
*/
}
