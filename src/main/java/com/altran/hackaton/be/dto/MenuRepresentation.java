package com.altran.hackaton.be.dto;

import com.altran.hackaton.be.model.RestaurantMenu;

public class MenuRepresentation {

	private String menuId;

	private String restaurantId;

	private String name;

	private Double value;

	private String image;

	private int quantity;

	public MenuRepresentation(String menuId, String restaurantId, String name, Double value, String image) {
		this.menuId = menuId;
		this.restaurantId = restaurantId;
		this.name = name;
		this.value = value;
		this.image = image;
		this.quantity = 0;
	}

	public String getMenuId() {
		return menuId;
	}

	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}

	public String getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(String restaurantId) {
		this.restaurantId = restaurantId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public static MenuRepresentation from(RestaurantMenu menu) {
		return new MenuRepresentation(
				menu.getIdentifier(),
				menu.getRestaurantId(),
				menu.getName(),
				menu.getValue(),
				menu.getImage());
	}
}
