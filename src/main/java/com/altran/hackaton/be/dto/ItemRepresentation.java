package com.altran.hackaton.be.dto;

import com.altran.hackaton.be.model.Item;

public class ItemRepresentation {

	private String menuId;

	private int quantity;

	private String orderId;

	public ItemRepresentation(String menuId, int quantity,String orderId) {
		this.menuId = menuId;
		this.quantity = quantity;
		this.orderId = orderId;
	}

	public String getMenuId() {
		return menuId;
	}

	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public static ItemRepresentation from(Item menu) {
		return new ItemRepresentation(
				menu.getIdentifier(),
				menu.getQuantity(),
				menu.getParent());
	}
}
