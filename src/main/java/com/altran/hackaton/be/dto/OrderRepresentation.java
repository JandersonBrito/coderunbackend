package com.altran.hackaton.be.dto;

import com.altran.hackaton.be.model.Order;

import java.util.List;

public class OrderRepresentation {

	private String orderId;

	private String status;

	private String restaurantId;

	private String userId;

	private List<ItemRepresentation> items;

	public OrderRepresentation(String orderId, String status, String restaurantId, String userId) {
		this.orderId = orderId;
		this.status = status;
		this.restaurantId = restaurantId;
		this.userId = userId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(String restaurantId) {
		this.restaurantId = restaurantId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public List<ItemRepresentation> getItems() {
		return items;
	}

	public void setItems(List<ItemRepresentation> items) {
		this.items = items;
	}

	public static OrderRepresentation from(Order order) {
		return new OrderRepresentation(
				order.getIdentifier(),
				order.getStatus(),
				order.getRestaurantId(),
				order.getUserId());
	}
}
