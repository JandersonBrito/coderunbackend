package com.altran.hackaton.be.service;

import com.altran.hackaton.be.dao.MenuRepository;
import com.altran.hackaton.be.dao.RestaurantRepository;
import com.altran.hackaton.be.dto.MenuRepresentation;
import com.altran.hackaton.be.dto.RestaurantRepresentation;
import com.altran.hackaton.be.model.Location;
import com.altran.hackaton.be.model.Restaurant;
import com.altran.hackaton.be.model.RestaurantMenu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MenuService {
	
	@Autowired
	private MenuRepository menuRepo;

	public List<MenuRepresentation> listMenu(String token, String restaurantId){
		Collection<RestaurantMenu> menu = new ArrayList<>();
		menuRepo.findAllByRestaurantId(restaurantId).forEach(menu::add);
		return menu
				.stream()
				.map(rest -> MenuRepresentation.from(rest))
				.collect(Collectors.toList());
	}

	public List<MenuRepresentation> listMenu(String token){
		Collection<RestaurantMenu> menu = new ArrayList<>();
		menuRepo.findAll().forEach(menu::add);
		return menu
				.stream()
				.map(rest -> MenuRepresentation.from(rest))
				.collect(Collectors.toList());
	}
	
}
