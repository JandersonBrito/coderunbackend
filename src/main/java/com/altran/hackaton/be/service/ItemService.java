package com.altran.hackaton.be.service;

import com.altran.hackaton.be.dao.ItemRepository;
import com.altran.hackaton.be.dao.MenuRepository;
import com.altran.hackaton.be.dto.ItemRepresentation;
import com.altran.hackaton.be.dto.MenuRepresentation;
import com.altran.hackaton.be.model.Item;
import com.altran.hackaton.be.model.RestaurantMenu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ItemService {
	
	@Autowired
	private ItemRepository itemRepo;

	public List<ItemRepresentation> listMenu(String token, String orderId){
		Collection<Item> item = new ArrayList<>();
		itemRepo.findAllByParent(orderId).forEach(item::add);
		return item
				.stream()
				.map(rest -> ItemRepresentation.from(rest))
				.collect(Collectors.toList());
	}

	public List<ItemRepresentation> listMenu(String token){
		Collection<Item> menu = new ArrayList<>();
		itemRepo.findAll().forEach(menu::add);
		return menu
				.stream()
				.map(rest -> ItemRepresentation.from(rest))
				.collect(Collectors.toList());
	}
	
}
