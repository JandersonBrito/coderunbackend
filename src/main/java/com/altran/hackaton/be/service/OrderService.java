package com.altran.hackaton.be.service;

import com.altran.hackaton.be.dao.ItemRepository;
import com.altran.hackaton.be.dao.OrderRepository;
import com.altran.hackaton.be.dao.RestaurantRepository;
import com.altran.hackaton.be.dto.ItemRepresentation;
import com.altran.hackaton.be.dto.OrderRepresentation;
import com.altran.hackaton.be.dto.RestaurantRepresentation;
import com.altran.hackaton.be.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class OrderService {

    @Autowired
    private OrderRepository orderRepo;

    @Autowired
    private ItemRepository itemRepo;

    public List<OrderRepresentation> listOrder(String token){
        Collection<Order> order = new ArrayList<>();
        orderRepo.findAll().forEach(order::add);
        return order
                .stream()

                .map(rest -> {
                    OrderRepresentation orderRep = OrderRepresentation.from(rest);

                    List<Item> items = itemRepo.findAllByParent(orderRep.getOrderId());
                    List <ItemRepresentation> iRep = orderRep.getItems();

                    if(iRep!=null) {
                        for (ItemRepresentation itemRep : iRep) {
                            Item item = new Item();
                            item.setIdentifier(itemRep.getMenuId());
                            item.setParent(itemRep.getOrderId());
                            item.setQuantity(itemRep.getQuantity());
                            itemRepo.save(item);
                        }
                        orderRep.setItems(iRep);
                    }

                    return orderRep;
                })

                .collect(Collectors.toList());
    }

    public List<OrderRepresentation> listOrder(String token,String userId){
        Collection<Order> order = new ArrayList<>();
        orderRepo.findAllByUserId(userId).forEach(order::add);
        return order
                .stream()

                .map(rest -> {
                    OrderRepresentation orderRep = OrderRepresentation.from(rest);

                    List<Item> items = itemRepo.findAllByParent(orderRep.getOrderId());
                    List <ItemRepresentation> iRep = orderRep.getItems();

                    if(iRep!=null) {
                        for (ItemRepresentation itemRep : iRep) {
                            Item item = new Item();
                            item.setIdentifier(itemRep.getMenuId());
                            item.setParent(itemRep.getOrderId());
                            item.setQuantity(itemRep.getQuantity());
                            itemRepo.save(item);
                        }
                        orderRep.setItems(iRep);
                    }
                    return orderRep;
                })

                .collect(Collectors.toList());
    }


   /* public List<OrderRepresentation> listOrder(String token,String restaurantId){
        Collection<Order> order = new ArrayList<>();
        orderRepo.findAllByParent(restaurantId).forEach(order::add);
        return order
                .stream()
                .map(rest -> OrderRepresentation.from(rest))
                .collect(Collectors.toList());
    }*/

    public List<OrderRepresentation> findOrder(String token,String orderId){
        Collection<Order> order = new ArrayList<>();
        orderRepo.findAllByIdentifier(orderId).forEach(order::add);
        return order
                .stream()
                .map(rest -> OrderRepresentation.from(rest))
                .collect(Collectors.toList());
    }


    public OrderRepresentation save(OrderRepresentation orderRep){
        Order order = new Order ();
        order.setIdentifier(orderRep.getOrderId());
        order.setRestaurantId(orderRep.getRestaurantId());
        order.setStatus(orderRep.getStatus());
        order.setUserId(orderRep.getUserId());
        Order orderRet = orderRepo.save(order);

        for(ItemRepresentation itemrep: orderRep.getItems() ){
            Item item = new Item();
            item.setIdentifier(itemrep.getMenuId());
            item.setParent(itemrep.getOrderId());
            item.setQuantity(itemrep.getQuantity());
            itemRepo.save(item);
        }

        orderRep.setOrderId(orderRet.getIdentifier());

        return orderRep;

    }
    public OrderRepresentation save(String orderId , OrderRepresentation orderRep){
        Order order = new Order();
        order.setIdentifier(orderId);
        order.setRestaurantId(orderRep.getRestaurantId());
        order.setStatus(orderRep.getStatus());
        order.setUserId(orderRep.getUserId());
        Order orderRet = orderRepo.save(order);

        for(ItemRepresentation itemrep: orderRep.getItems() ){
            Item item = new Item();
            item.setIdentifier(UUID.randomUUID().toString());
            item.setParent(orderId);
            item.setQuantity(itemrep.getQuantity());
            item.setMenuId(itemrep.getMenuId());
            itemRepo.save(item);
        }

        orderRep.setOrderId(orderRet.getIdentifier());

        return orderRep;

    }

    public void delete(String orderId){
         orderRepo.deleteById(orderId);
        List<Item> itens = itemRepo.findAllByParent(orderId);
        for(Item item: itens ){
            itemRepo.deleteById(item.getIdentifier());
        }

    }

}
