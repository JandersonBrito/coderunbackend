package com.altran.hackaton.be.dao;

import com.altran.hackaton.be.model.Order;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OrderRepository extends CrudRepository<Order, String>{

    List<Order> findAllByRestaurantId(String restaurantId);
    List<Order> findAllByIdentifier(String orderId);
    List<Order> findAllByUserId(String userId);


}
