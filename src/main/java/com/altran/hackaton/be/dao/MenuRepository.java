package com.altran.hackaton.be.dao;

import com.altran.hackaton.be.model.Restaurant;
import com.altran.hackaton.be.model.RestaurantMenu;
import com.altran.hackaton.be.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface MenuRepository extends CrudRepository<RestaurantMenu, String>{

    List<RestaurantMenu> findAllByRestaurantId(String restaurantId);

}
