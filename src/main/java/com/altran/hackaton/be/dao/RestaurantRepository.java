package com.altran.hackaton.be.dao;

import com.altran.hackaton.be.model.Restaurant;
import org.springframework.data.repository.CrudRepository;

public interface RestaurantRepository extends CrudRepository<Restaurant, String>{

  //  User findAllOrdersById(int id);

}
