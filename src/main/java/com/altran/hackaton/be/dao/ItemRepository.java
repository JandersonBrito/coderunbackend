package com.altran.hackaton.be.dao;

import com.altran.hackaton.be.model.Item;
import com.altran.hackaton.be.model.Order;
import com.altran.hackaton.be.model.RestaurantMenu;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ItemRepository extends CrudRepository<Item, String>{

    List<Item> findAllByParent(String parent);
    List<Item> findAllByIdentifier(String orderId);

}
