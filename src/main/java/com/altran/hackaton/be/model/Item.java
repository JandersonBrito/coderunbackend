package com.altran.hackaton.be.model;

import javax.persistence.*;

@Entity
@Table(name = "ITEMS")
public class Item {

    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ITEM_ID")
    private String identifier;

    @Column(name = "MENU_ID")
    private String menuId;

    @Column(name = "ORDER_ID")
    private String parent;

    @Column(name = "QUANTITY")
    private int quantity;

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }
}
